﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace LVL2_ASPNet_MVC_01.Controllers
{
    public class CodingIdController : Controller
    {
        // GET: CodingId
        public ActionResult CodingIDMessage()
        {
            return View();
        }
        [Route("CodingId/Hola")]
        public ActionResult GoodBye() 
        {
            return View();
        }
    }
}